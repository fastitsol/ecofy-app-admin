<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Recommandation extends Model
{
    protected $fillable = [
        'type','image','name','description','carbonFootprint','rating'
    ];
}
