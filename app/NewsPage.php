<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NewsPage extends Model
{
    protected $fillable = [
        'sliderImg', 'title', 'place', 'dateTime','description'
    ];
}
