<?php

namespace App\Http\Controllers;

use App\Tag;
use App\Role;
use App\User;
use App\Category;
use App\FaqTopic;
use App\Faq;
use App\PaypalInfo;
use App\ContactUs;
use App\NewsPage;
use App\Recommandation;
use App\About;
use App\Question;
use App\QuestionChoice;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{
    protected $url = "http://app.ecofy.online/";
    public function index(Request $request){
        // first check if you are loggedin and admin user ... 
        if(!Auth::check() && $request->path() != 'login'){
            return redirect('/login');
        }
        if(!Auth::check() && $request->path() == 'login' ){
            return view('welcome');
        }
        // you are already logged in... so check for if you are an admin user.. 
        $user = Auth::user();
        if($user->userType =='User'){
            return redirect('/login');
        }
        if($request->path() == 'login'){
            return redirect('/');
        }
        return view('welcome');
        
    }
    public function getContact(){
        return ContactUs::orderBy('id', 'desc')->get();
    }
    
    public function getmessage(Request $request){
        return ContactUs::where('id',$request->msgId)->orderBy('id', 'desc')->first();
    }
    
    public function logout(){
        Auth::logout();
        return redirect('/login');
    }
    public function upload(Request $request){
        $url = "http://app.ecofy.online";
        $this->validate($request, [
            'file' => 'required|mimes:jpeg,jpg,png'
        ]);
        $picName = time().'.'.$request->file->extension();
        $request->file->move(public_path('uploads'),$picName );
        // return $url."/uploads/$picName";
        return $picName;
    }
    // public function upload(Request $request){
    //     // \Log::info($request->all());
    //     // $url = $request->url;
    //     request()->file('attachment')->store('uploads');
    //     $attachment= $request->attachment->hashName();
    //     $attachment= $this->$url."/uploads/$attachment";
    //     /*update the profile pic*/
    //     //return Gallery::create($data);
    //     return response()->json([
    //         'attachment'=> $attachment
    //     ],200);
    // }
    public function deleteImage(Request $request){
        $fileName = $request->imageName; 
        $this->deleteFileFromServer($fileName, false);
        return 'done';
    }
    public function deleteFileFromServer($fileName, $hasFullPath = false){
        if(!$hasFullPath){
            $filePath = public_path().'/uploads/'.$fileName;
        }
        if(file_exists($filePath)){
            @unlink($filePath);
        }
        return;
    }
    public function addCategory(Request $request){
        // validate request 
        $this->validate($request, [
            'categoryName' => 'required',
            'iconImage' => 'required',
        ]);
        return Category::create([
            'categoryName' => $request->categoryName,
            'iconImage' => $request->iconImage,
        ]);
    }
    public function addNews(Request $request){
        // validate request 
        $this->validate($request, [
            'title' => 'required',
            'place' => 'required',
            'dateTime' => 'required',
            'description' => 'required',
            'sliderImg' => 'required',
        ]);
        return NewsPage::create([
            'title' => $request->title,
            'place' => $request->place,
            'dateTime' => $request->dateTime,
            'description' => $request->description,
            'sliderImg' => $request->sliderImg,
        ]);
    }
    public function create_payPalInfo(Request $request){
        // validate request 
        $this->validate($request, [
            'client_id' => 'required',
            'secert_key' => 'required',
            
        ]);
        return PaypalInfo::create([
            'client_id' => $request->client_id,
            'secret_key' => $request->secert_key,
            
        ]);
    }
    public function totalNews(){
        return NewsPage::count();
    }
    
    public function addRecommandation(Request $request){
        // validate request 
        $this->validate($request, [
            'type' => 'required',
            'image' => 'required',
            'name' => 'required',
            'description' => 'required',
            'carbonFootprint' => 'required',
            'rating' => 'required',
        ]);
        return Recommandation::create([
            'type' => $request->type,
            'image' => $request->image,
            'name' => $request->name,
            'description' => $request->description,
            'carbonFootprint' => $request->carbonFootprint,
            'rating' => $request->rating,
        ]);
    }
    public function editRecommandation(Request $request){
        // validate request 
        $this->validate($request, [
            'type' => 'required',
            'image' => 'required',
            'name' => 'required',
            'description' => 'required',
            'carbonFootprint' => 'required',
            'rating' => 'required',
        ]);
        return Recommandation::where('id', $request->id)->update([
            'type' => $request->type,
            'image' => $request->image,
            'name' => $request->name,
            'description' => $request->description,
            'carbonFootprint' => $request->carbonFootprint,
            'rating' => $request->rating,
        ]);
    }
    public function getFoodRecommendation(){
        return Recommandation::where('type','food')->orderBy('id', 'desc')->get();
    }
    public function getTotalFoodRecommendation(){
        return Recommandation::where('type','food')->count();
    }
    public function getTotaltransportRecommendation(){
        return Recommandation::where('type','transport')->count();
    }
    public function getTotalutilityRecommendation(){
        return Recommandation::where('type','utility')->count();
    }
    public function getTransportRecommendation(){
        return Recommandation::where('type','transport')->orderBy('id', 'desc')->get();
    }
    public function getUtilityRecommendation(){
        return Recommandation::where('type','utility')->orderBy('id', 'desc')->get();
    }
    public function getDescOfRecommandation(Request $request){
        return Recommandation::where('id',$request->Id)->orderBy('id', 'desc')->first();
    }
    public function deleteRecommandation(Request $request){
        // first delete the original file from the server 
        $this->deleteFileFromServer($request->image); 
        // validate request 
        $this->validate($request, [
            'id' => 'required', 
        ]);
        return Recommandation::where('id', $request->id)->delete();
    }
    public function addQuestion(Request $request){
        // validate request 
        $this->validate($request, [
            'question' => 'required',
            // 'hint' => 'required',
        ]);
        return Question::create([
            'question' => $request->question,
            'hint' => $request->hint,
        ]);
    }
    public function addChoice(Request $request){
        // validate request 
        $this->validate($request, [
            'question_id' => 'required',
            'choice' => 'required',
            // 'is_right_choice' => 'required',
        ]);
        $questionChoice = QuestionChoice::create([
            'question_id' => $request->question_id,
            'choice' => $request->choice,
            'is_right_choice' => $request->is_right_choice,
        ]);
        return QuestionChoice::where('id',$questionChoice->id)->with('question')->orderBy('id', 'asc')->first();
    }
    public function deleteChoice(Request $request){
        $data = $request->all();
        return QuestionChoice::where('id',$data['id'])->delete();
    }
    public function editChoice(Request $request){
        // validate request 
        $this->validate($request, [
            'question_id' => 'required',
            'choice' => 'required',
            'is_right_choice' => 'required',
        ]);
        $questionChoice = QuestionChoice::where('id', $request->id)->update([
            'question_id' => $request->question_id,
            'choice' => $request->choice,
            'is_right_choice' => $request->is_right_choice,
        ]);
        return QuestionChoice::where('id',$request->id)->with('question')->orderBy('id', 'asc')->first();
        
    }
    public function getChoice(){
        return QuestionChoice::with('question')->orderBy('id', 'asc')->get();
    }
    public function getQuestionChoice(){
        return Question::with('choices')->orderBy('id', 'asc')->get();
    }
    public function addFAQ(Request $request){
        // validate request 
        $this->validate($request, [
            'topic_id' => 'required',
            'question' => 'required',
            'answer' => 'required',
        ]);
        return Faq::create([
            'topic_id' => $request->topic_id,
            'question' => $request->question,
            'answer' => $request->answer,
            
        ]);
    }
    public function editFAQ(Request $request){
        $this->validate($request, [
            'topic_id' => 'required',
            'question' => 'required',
            'answer' => 'required',
        ]);
        return Faq::where('id', $request->id)->update([
            'topic_id' => $request->topic_id,
            'question' => $request->question,
            'answer' => $request->answer,
        ]);
    }
    public function deleteFAQ(Request $request){
        $this->validate($request, [
            'id' => 'required', 
        ]);
        return Faq::where('id', $request->id)->delete();
    }
    public function getNews(){
        return NewsPage::orderBy('id', 'desc')->get();
    }
    public function get_PayPalInfo(){
        return PaypalInfo::orderBy('id', 'desc')->get();
    }
    
    public function getQuestion(){
        return Question::orderBy('id', 'desc')->get();
    }
    public function getAbout(){
        return About::orderBy('id', 'desc')->get();
    }
    public function getFaqTopic(){
        return FaqTopic::orderBy('id', 'desc')->get();
    }
    public function getFaq(){
        return Faq::with('faq_topic')->orderBy('id', 'asc')->get();
    }
    
    public function storeFAQtopic(Request $request)
    {
        $data = $request->all();
        return FaqTopic::create($data);
    }
    public function editFAQtopic(Request $request){
        $data = $request->all();
        return FaqTopic::where('id',$data['id'])->update([
            'name' => $request->name,
        ]);
    }
    public function deleteFAQtopic(Request $request){
        $data = $request->all();
        return FaqTopic::where('id',$data['id'])->delete();
    }
    
    public function getdescription(Request $request){
        return NewsPage::where('id',$request->newsId)->orderBy('id', 'desc')->first();
    }
    
    public function getAboutDescription(Request $request){
        return About::where('id',$request->aboutId)->orderBy('id', 'desc')->first();
    }
    public function getCategory(){
        return Category::orderBy('id', 'desc')->get();
    }
    
    public function editCategory(Request $request){
        // validate request 
        $this->validate($request, [
            'categoryName' => 'required',
            'iconImage' => 'required',
        ]);
        return Category::where('id', $request->id)->update([
            'categoryName' => $request->categoryName,
            'iconImage' => $request->iconImage,
        ]);
    }
    public function editNews(Request $request){
        // validate request 
        $this->validate($request, [
            'title' => 'required',
            'place' => 'required',
            'dateTime' => 'required',
            'description' => 'required',
            'sliderImg' => 'required',
        ]);
        return NewsPage::where('id', $request->id)->update([
            'title' => $request->title,
            'place' => $request->place,
            'dateTime' => $request->dateTime,
            'description' => $request->description,
            'sliderImg' => $request->sliderImg,
        ]);
    }
    public function edit_paypal(Request $request){
        // validate request 
        $this->validate($request, [
            'client_id' => 'required',
            'secret_key' => 'required'
           
        ]);
        return PaypalInfo::where('id', $request->id)->update([
            'client_id' => $request->client_id,
            'secret_key' => $request->secret_key
            
            
        ]);
    }
   
    public function editQuestion(Request $request){
        // validate request 
        $this->validate($request, [
            'question' => 'required',
        ]);
        return Question::where('id', $request->id)->update([
            'question' => $request->question,
            'hint' => $request->hint,
        ]);
    }
    
    public function editAbout(Request $request){
        // validate request 
        $this->validate($request, [
            'description' => 'required',
        ]);
        return About::where('id', $request->id)->update([
            'description' => $request->description,
        ]);
    }
    public function deleteCategory(Request $request){
        // first delete the original file from the server 
        $this->deleteFileFromServer($request->iconImage); 
        // validate request 
        $this->validate($request, [
            'id' => 'required', 
        ]);
        return Category::where('id', $request->id)->delete();
    }
    public function deleteContact(Request $request){
        // first delete the original file from the server 
        // $this->deleteFileFromServer($request->iconImage); 
        // validate request 
        $this->validate($request, [
            'id' => 'required', 
        ]);
        return ContactUs::where('id', $request->id)->delete();
    }
    public function deleteNews(Request $request){
        // first delete the original file from the server 
        $this->deleteFileFromServer($request->sliderImg); 
        // validate request 
        $this->validate($request, [
            'id' => 'required', 
        ]);
        return NewsPage::where('id', $request->id)->delete();
    }
    public function delete_PaypalInfo(Request $request){
        // first delete the original file from the server 
        // $this->deleteFileFromServer($request->sliderImg); 
        // validate request 
        $this->validate($request, [
            'id' => 'required', 
        ]);
        return PaypalInfo::where('id', $request->id)->delete();
    }
    
    public function deleteQuestion(Request $request){
        // first delete the original file from the server 
        // $this->deleteFileFromServer($request->sliderImg); 
        // validate request 
        $this->validate($request, [
            'id' => 'required', 
        ]);
        return question::where('id', $request->id)->delete();
    }
    public function createUser(Request $request){
         // validate request 
         $this->validate($request, [
            'fullName' => 'required',
            'email' => 'bail|required|email|unique:users',
            'password' => 'bail|required|min:6',
            'role_id' => 'required',
        ]);
        $password = bcrypt($request->password);
        $user = User::create([
            'fullName' => $request->fullName,
            'email' => $request->email,
            'password' => $password,
            'userType' => $request->userType,
        ]);
        return $user;
    }
    public function editUser(Request $request){
         // validate request 
         $this->validate($request, [
            'fullName' => 'required',
            'email' => "bail|required|email|unique:users,email,$request->id",
            'password' => 'min:6',
            'userType' => 'required',
        ]);
        $data = [
            'fullName' => $request->fullName,
            'email' => $request->email,
            'userType' => $request->userType,
        ];
        if($request->password){
            $password = bcrypt($request->password);
            $data['password'] = $password;
        }
        $user = User::where('id', $request->id)->update($data);
        return $user;
    }

    public function getUsers(){
        return User::get();
    }
    public function adminLogin(Request $request){
         // validate request 
        $this->validate($request, [
            'email' => 'bail|required|email',
            'password' => 'bail|required|min:6',
        ]);
        if(Auth::attempt(['email' => $request->email, 'password' => $request->password])){
            $user = Auth::user();
            if($user->userType == 'Admin'){
                return response()->json([
                    'msg' => 'You are logged in', 
                    'user' => $user
                ]);
            }
            Auth::logout();
            return response()->json([
                'msg' => 'Incorrect login details', 
            ], 401);
            
        }else{
            return response()->json([
                'msg' => 'Incorrect login details', 
            ], 401);
        }

    }

   



}


