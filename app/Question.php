<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $fillable = [
        'question','hint'
    ];
    public function choices(){
        return $this->hasMany('App\QuestionChoice','question_id');
    }
}
