<?php

use App\Http\Middleware\AdminCheck;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('app')->middleware([AdminCheck::class])->group(function(){
   
    Route::get('/get_Contact', 'AdminController@getContact');
    Route::get('/get_news', 'AdminController@getNews');
    Route::get('/get_PayPalInfo', 'AdminController@get_PayPalInfo');
    Route::get('/get_question', 'AdminController@getQuestion');
    Route::get('/get_about', 'AdminController@getAbout');
    Route::get('/get_faqTopic', 'AdminController@getFaqTopic');
    Route::get('/getFaq', 'AdminController@getFaq');
    Route::get('/getChoice', 'AdminController@getChoice');
    Route::get('/getQuestionChoice', 'AdminController@getQuestionChoice');
    Route::post('/storeFAQtopic', "AdminController@storeFAQtopic");
    Route::post('/deleteFAQtopic', "AdminController@deleteFAQtopic");
    Route::post('/deleteChoice', "AdminController@deleteChoice");
    Route::post('/editFAQtopic', "AdminController@editFAQtopic");
    Route::get('/getmessage', 'AdminController@getmessage');
    Route::get('/getdescription', 'AdminController@getdescription');
    Route::get('/getAboutDescription', 'AdminController@getAboutDescription');
    Route::post('/upload', 'AdminController@upload');
    Route::post('/delete_image', 'AdminController@deleteImage');
    Route::post('/create_category', 'AdminController@addCategory');
    Route::post('/create_news', 'AdminController@addNews');
    Route::post('/create_payPalInfo', 'AdminController@create_payPalInfo');
    Route::get('/totalNews', 'AdminController@totalNews');

    Route::get('/getDescOfRecommandation', 'AdminController@getDescOfRecommandation');
    Route::post('/create_recommandation', 'AdminController@addRecommandation');
    Route::post('/editRecommandation', 'AdminController@editRecommandation');
    Route::get('/getFoodRecommendation', 'AdminController@getFoodRecommendation');
    Route::get('/getTotalFoodRecommendation', 'AdminController@getTotalFoodRecommendation');
    Route::get('/getTotaltransportRecommendation', 'AdminController@getTotaltransportRecommendation');
    Route::get('/getTotalutilityRecommendation', 'AdminController@getTotalutilityRecommendation');
    Route::get('/getTransportRecommendation', 'AdminController@getTransportRecommendation');
    Route::get('/getUtilityRecommendation', 'AdminController@getUtilityRecommendation');
    Route::post('/deleteRecommandation', 'AdminController@deleteRecommandation');

    Route::post('/create_question', 'AdminController@addQuestion');
    Route::post('/create_choice', 'AdminController@addChoice');
    Route::post('/create_FAQ', 'AdminController@addFAQ');
    Route::get('/get_category', 'AdminController@getCategory');
    Route::post('/edit_category', 'AdminController@editCategory');
    Route::post('/edit_news', 'AdminController@editNews');
    Route::post('/edit_paypal', 'AdminController@edit_paypal');
    Route::post('/edit_FAQ', 'AdminController@editFAQ');
    Route::post('/edit_question', 'AdminController@editQuestion');
    Route::post('/edit_choice', 'AdminController@editChoice');
    Route::post('/edit_about', 'AdminController@editAbout');
    Route::post('/delete_category', 'AdminController@deleteCategory');
    Route::post('/delete_contact', 'AdminController@deleteContact');
    Route::post('/delete_news', 'AdminController@deleteNews');
    Route::post('/delete_PaypalInfo', 'AdminController@delete_PaypalInfo');
    Route::post('/delete_FAQ', 'AdminController@deleteFAQ');
    Route::post('/delete_question', 'AdminController@deleteQuestion');
    Route::post('/create_user', 'AdminController@createUser');
    Route::get('/get_users', 'AdminController@getUsers');
    Route::post('/edit_user', 'AdminController@editUser');
    Route::post('/admin_login', 'AdminController@adminLogin');
});


Route::get('/logout', 'AdminController@logout');
Route::get('/', 'AdminController@index');
Route::any('{slug}', 'AdminController@index');
