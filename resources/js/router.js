
import Router from 'vue-router'
import Vue from 'vue'
Vue.use(Router)
// admin project pages 
import home from './components/pages/home'
import category from './admin/pages/category'
import contact_us from './admin/pages/contact_us'
import about from './admin/pages/about'
import FAQ_topic from './admin/pages/FAQ_topic'
import mainFAQ from './admin/pages/mainFAQ'
import news_page from './admin/pages/news'
import food_recommandation from './admin/pages/food_recommandation'
import transport_recommandation from './admin/pages/transport_recommandation'
import utility_recommandation from './admin/pages/utility_recommandation'
import question from './admin/pages/question'
import choice from './admin/pages/choice'
import adminusers from './admin/pages/adminusers'
import paypal_info from './admin/pages/paypal_info'
import login from './admin/pages/login'
const routes = [
    //projects routes....

    {
        path: '/', 
        component: home, 
        name: 'home'
       
    },

    {
        path: '/category', 
        component: category, 
        name: 'category'
       
    },
    {
        path: '/contact_us', 
        component: contact_us, 
        name: 'contact_us'
       
    },
    {
        path: '/paypal_info', 
        component: paypal_info, 
        name: 'paypal_info'
       
    },
    {
        path: '/about', 
        component: about, 
        name: 'about'
       
    },
    {
        path: '/FAQ_topic', 
        component: FAQ_topic, 
        name: 'FAQ Topic'
       
    },
    {
        path: '/mainFAQ', 
        component: mainFAQ, 
        name: 'FAQ'
       
    },
    {
        path: '/news_page', 
        component: news_page, 
        name: 'news_page'
       
    },
    {
        path: '/food_recommandation', 
        component: food_recommandation, 
        name: 'food_recommandation'
       
    },
    {
        path: '/transport_recommandation', 
        component: transport_recommandation, 
        name: 'transport_recommandation'
       
    },
    {
        path: '/utility_recommandation', 
        component: utility_recommandation, 
        name: 'utility_recommandation'
       
    },
    {
        path: '/question', 
        component: question, 
        name: 'question'
       
    },
    {
        path: '/choice', 
        component: choice, 
        name: 'choice'
       
    },
    {
        path: '/adminusers', 
        component: adminusers, 
        name: 'adminusers'
       
    },
    {
        path: '/login', 
        component: login, 
        name: 'login'
    },




]



export default new Router({
    mode: 'history', 
    routes

})