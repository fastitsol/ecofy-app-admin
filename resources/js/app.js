// require('./bootstrap');
// window.Vue = require('vue')
// import router from './router'
// import store from './store'
// import ViewUI from 'view-design';
// import 'view-design/dist/styles/iview.css';
// // import Vue from 'vue';
// import iView from 'iview';
// import locale from 'iview/dist/locale/en-US';
// Vue.use(iView, {locale: locale});
// Vue.use(ViewUI);
// import common from './common'
// Vue.mixin(common)

// Vue.component('mainapp', require('./components/mainapp.vue').default)
// const app = new Vue({
//     el: '#app', 
//     router, 
//     store
// })
require('./bootstrap');
window.Vue = require('vue')
import router from './router'
import store from './store'
import ViewUI from 'view-design';


Vue.use(ViewUI);
import common from './common'
Vue.mixin(common)


import iView from 'iview';
import 'iview/dist/styles/iview.css';
import locale from 'iview/dist/locale/en-US';
Vue.use(iView, { locale });
import Vue from 'vue';

Vue.component('mainapp', require('./components/mainapp.vue').default)
const app = new Vue({
    el: '#app', 
    router, 
    store
})